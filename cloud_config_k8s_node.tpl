#cloud-config
users:
  - name: audron
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: users, admin, wheel, docker
    lock_passwd: false
    shell: /bin/bash
    ssh_import_id: None
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCjAdBaOutOhbkzjn/xnwG03QOuIhkJmIcupZ/tKNgDHRRwEw5i3LV31joZ1NLo3wqbdAi5cTAW65q13Zexwjpf3GhFFBr7sQA60fvc176fEgeZ+i8Q+kYtccHwzsOlkGfUe/d8hsEpt2AVJiEkg+10cqgr0CN6KxcXQenR6WWW3RWSlHfXnjDAos8KlzoJ1mE3cMQ+43CgBieWXOVOjRaA/61R8nadd8zR0e4JvKf0RS1RHSc7hxQdfLUzmcjFMXmHcx20N7tyqDZwvBqdGA1OQuGCkIhXO7c9ONfpOGonj6sKEVcprtmqixHVE51WZerKHypz0ffZwlHEDtnQm2yZURgo4tLla1Nrq6CvlfgPWK3xfTwU/BBEPbB8HtxIDiFqwPovams37oYPc3jxNUsSvuzrox7X3bj2NrMPU9nE119y1UqPL0gZ3WkgLqgWa6MSrFf0dljEkDE/2SuxVpn26RRka4xwF0OHE7n+YT27kqtrAAUYmMnkJlHO+TN6BgGtQuUKfP1OmwQrgxYDND252YGRgHtfL9ZBjs3b1meXZed32CRzubwqqZ6Fhp1t6k6OSKGJgfx1iL7MtiWVr0KoSZG58nR8b6QEdfNoovYj5VsQKs9z9NRSDmZxnSSbMzFwWG3GeFmaFJqOZNYaYyINCij2wavPQjxNCbdb3DnhsQ== cardno:000608699544

package_update: false
package_upgrade: true

write_files:
    - encoding: b64
      path: /etc/sysctl.d/99-kubernetes-cri.conf
      content: |
          bmV0LmJyaWRnZS5icmlkZ2UtbmYtY2FsbC1pcHRhYmxlcyAgPSAxCm5ldC5pcHY0LmlwX2Zvcndh
          cmQgICAgICAgICAgICAgICAgID0gMQpuZXQuYnJpZGdlLmJyaWRnZS1uZi1jYWxsLWlwNnRhYmxl
          cyA9IDEK
# This is for configuring a cloud provider
# add your own base64 encoded files here or leave commented
#    - encoding: b64
#      path: /etc/kubernetes/cloud.conf
#      content: | # leave commented out or add base64 encoded content here
#    - encoding: b64
#      path: /etc/kubernetes/kubeadm_custom.conf
#      content: |

runcmd:
    - apt-get update
    - apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    - curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    - add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
    - apt-get update
    - modprobe overlay
    - modprobe br_netfilter
    - sysctl --system
    - apt-get install -y containerd.io
    - mkdir -p /etc/containerd
    - containerd config default > /etc/containerd/config.toml
    - systemctl restart containerd
    - apt-get install -y kubelet kubeadm kubectl
    - apt-mark hold kubelet kubeadm kubectl
# Add your own values here to join nodes
    - kubeadm join <ip> --token <token> --discovery-token-ca-cert-hash <discovery-token>
